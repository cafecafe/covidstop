
import XLSX from 'xlsx';
import {entozh} from './entozh';
import {Button,Col} from 'react-bootstrap';
import {Upload } from 'antd';
import {config} from "./config";
export default function ExcelLoad(props) {

	const defaultObj = {
			      serial : "" ,
			      name : "" ,
			      amount : "" ,
			      bill_time : new Date().toLocaleDateString(),
			      sentence : "" ,
			      price : "" ,
			      size : "" ,
			      color : "" ,
			      style : "" ,
			      example : "" ,
			      remark : "" ,
			      saveAmount : "" ,
			      whole_sale : [],
			      member_sale : [],
			      member_order : [],
			      client_order : [],
			      manu_order : []

	}
	const uploadProps= {

		onRemove: file => {

		},
		accept: ".xls,.xlsx,application/vnd.ms-excel",
		beforeUpload: (file) => {
			const _this=this;
			const f = file;
			const reader = new FileReader();
			reader.onload = function (e) {
				const datas = e.target.result;
				console.log('e.target.result',datas);
				const workbook = XLSX.read(datas, {
					type: 'binary'
				});//嘗試解析datas
				const first_worksheet = workbook.Sheets[workbook.SheetNames[0]];//是工作簿中的工作表的有序列表
				let jsonArr = XLSX.utils.sheet_to_json(first_worksheet, { header: 1 });//將工作簿對象轉換為JSON對象數組
				jsonArr = handleImportedJson(jsonArr);
				console.log(jsonArr);
				jsonArr = handleOrderToArr(jsonArr);
				let apiFormatRequest = handleImportToDBFormat(jsonArr);


				console.log(apiFormatRequest);
				if(jsonArr.length ==0 ) return false;
				sync(apiFormatRequest)
				props.setState(jsonArr);
			};
			reader.readAsBinaryString(f);
			return false;
		},
		onChange : (e,s) => {
		}
	};
	const sync = (jsonArr) => {
			let importJsonArr = {
				import : jsonArr
			};
			console.log(JSON.stringify(importJsonArr));
		    fetch(config.api.url+'/import', {
		        method: 'POST',
		        body: JSON.stringify(importJsonArr),
		        headers: new Headers({
		          'Content-Type': 'application/json'
		        })
		    }).then(res => {
		        if(res.status !=200) alert("載入失敗");
		        else alert("載入成功");
		    })


	}
	const handleImportedJson = (array) => {
		const header = array[0];//頭部數據 ["姓名",...]
		const newArray = [...array];
		newArray.splice(0, 1);//去除表頭
		const json = newArray.map((item, index) => {
			const newitem = {};
			item.forEach((im, i) => {
				const newKey = header[i] || i;
				newitem[newKey] = im
			})
			return newitem;
		});//將存在表頭定義字段的值賦值
		console.log('handleImportedJson', json);
		return json;
	}
	  const handleOrderToArr = (data) => {

	    return data.map( (item ,index )=> {
	      if(! item.serial) return defaultObj;
	      item['whole_sale'] = formatImportOrder(item['wholesaleOrderStr'].split("\n"));
	      item['member_sale'] = formatImportOrder(item['membersaleOrderStr'].split("\n"));
	      item['member_order'] = formatImportOrder(item['memberOrderStr'].split("\n"));
	      item['client_order'] = formatImportOrder(item['clientOrderStr'].split("\n"));
	      item['manu_order'] = formatImportOrder(item['manuOrderStr'].split("\n"));
	      console.log(item["member_sale"]);
	      return item;

	    })
	  }
	  const handleImportToDBFormat = (array) => {
	  	return array.map(item => (

	  		{
	  			main :{
			      serial : item.serial ,
			      name : item.name ,
			      amount : item.amount ,
			      bill_time : item.bill_time,
			      sentence : item.sentence ,
			      price : item.price ,
			      size : item.size ,
			      color : item.color ,
			      style : item.style ,
			      example : item.example ,
			      remark : item.remark ,
			      saveAmount : item.saveAmount 

	  			},
	  			order: {
			      whole_sale : item.whole_sale,
			      member_sale : item.member_sale,
			      member_order : item.member_order,
			      client_order : item.client_order,
			      manu_order : item.manu_order,

	  			}
	  		}
	  	))

	  }
	  const formatImportOrder = (tagOrder) => {
	  	if(tagOrder == "") return [];
	    tagOrder = tagOrder.map(item => {
	      let point = 0;
	      let splited_item = item.split(" ");

	      while(splited_item[point] == " " ) point++;
		  let tag = splited_item[point];
		  point++;
		  while(splited_item[point] == "" ) point++;
		  let value = splited_item[point];
		  if(!value) {
		  	value = tag;
		  	tag = "";
		  }
		  else {

		  value= value.split("\r")[0];
		  }
	      return { tag : tag ? tag :"" , value : value ? value: "0", id: null};

	    })
	    tagOrder = tagOrder.filter(item => item.value != "" || item.tag != "");
	    return tagOrder;
	  } 

	return (
		<>
	     <Upload {...uploadProps} >
	        <Button md={6} as={Col} size='sm' variant="primary" >匯入Excel</Button>
	      </Upload>

		</>

	);
}