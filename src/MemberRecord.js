import {useState ,useEffect } from 'react';
import {exportMember } from './ExcelSave';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {InputGroup, FormControl,Button,Table,Badge,Navbar,Modal} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {config} from "./config";
import { useHistory} from "react-router-dom";

function MemberRecord () {
  let history  = useHistory();
	const [members, setMembers] = useState([]);
	const [name , setName] = useState("");
	const [number , setNumber] = useState("");
	const [memberClass , setMemberClass] = useState("");
	const [fbName, setFbName] = useState("");
	const [lName, setLName] = useState("");
	const [address, setAddress] = useState("");
	const [destination ,setDestination] = useState("");
	const [birthday, setBirthday] = useState("");
	const [method, setMethod] = useState("");
	const [remark, setRemark] = useState("");
	const [shopGold, setShopGold] = useState("");
	const [phone ,setPhone] = useState("");
	const [email, setEmail] = useState("");
  const [MemberListShow, setMemberListShow] = useState(false);
  const [searchStr, setsearchStr ] = useState( "");
  const [memberid, setmemberid] = useState(0);
  const removeMember = (index,props) => {
    console.log(props);
    let right_arr = props.state.slice(index+1, props.state.length);
    let left_arr = props.state.slice(0);
    left_arr.length = index ;
    console.log(props.state);
    props.setState(left_arr.concat(right_arr));
    fetch(config.api.url+'/members/'+props.state[index].number, {
        method: 'DELETE',
        headers: new Headers({
          'Content-Type': 'application/json'
        })
    }).then(res => {
        if(res.status !=200) throw res.status; 
        return res.json()})
    .then(data => console.log(data))
    .catch(e => {
      alert("刪除失敗");
    })


  }
  const pullMember = (obj, index = members.length) => {
		setName(obj.name|| "")
		setNumber(obj.number|| "")
		setMemberClass(obj.member_class|| "")
		setFbName(obj.fb_name|| "")
		setLName(obj.l_name|| "")
		setAddress(obj.address|| "")
		setDestination(obj.destination|| "")
		setBirthday(obj.birthday|| "")
		setMethod(obj.method|| "")
		setRemark(obj.remark|| "")
		setShopGold(obj.shop_gold|| "")
		setPhone(obj.phone|| "")
		setEmail(obj.email|| "")
		setMemberListShow(false)
    setmemberid(index);
  }
  const pushMember = () => {
    console.log(number);
    if(!( name && number && phone)) return;
     const obj = {
				name : name,
				number : number,
				member_class : memberClass,
				fb_name : fbName,
				l_name : lName,
				address : address,
				destination : destination,
				birthday : birthday,
				method : method,
				remark : remark,
				shop_gold : shopGold,
				phone : phone,
				email : email
     };

    fetch(config.api.url+'/members', {
        method: 'POST',
        body: JSON.stringify(obj),
        headers: new Headers({
          'Content-Type': 'application/json'
        })
    }).then(res => {
        if(res.status !=200) throw res.status; 
        return res.json()})
    .then(data => {
      setMembers(preMembers => {
        let nextMembers = preMembers.slice(0);
        if(data.updated){
          nextMembers[memberid] =data.data;
        }
        else nextMembers[members.length] =obj;
        return nextMembers;

      });
      console.log(data);
      alert("儲存成功");
    })
    .catch(error => alert('儲存失敗'))
    setmemberid(memberid);
  }


  useEffect(() =>  
    fetch(config.api.url+'/members', {
        method: 'GET',
        headers: new Headers({
          'Content-Type': 'application/json'
        })
    })
    .then(res =>{
      if(res.status !=200) throw "儲存失敗"; 
      return res.json()
    })
    .then(data => {
        setMembers(data)} )
    ,[]);
	return (
		<>

			<Navbar expand="lg" variant="light" bg="light" sticky="top" as={Row} >
			<Col md={4} > <Badge variant="secondary">
			  <h1>會員資料紀錄表</h1> </Badge>  </Col>
      <Col md={1} align="center">
        
                  <Navbar.Brand >
                   <Button   onClick={() => history.push("/")}>
                    商品資料新增
                </Button> 
              </Navbar.Brand>
            
      </Col>
		  <Col md={1} align="center">
		    
		              <Navbar.Brand >
		               <Button onClick={() => setMemberListShow(true)}>
		              會員資料清單
		            </Button> 
		          </Navbar.Brand>
		        
		  </Col>
      <Col md={1}> <Button  variant="primary" onClick={()=> exportMember(members)}>匯出</Button></Col>
			</Navbar>
      <Row>
        <Col md="6">
          <Form>

            <Form.Row>

              <Col>
                <InputGroup size="sm">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-lg" >會員編號</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
                    onChange = {(event)=> setNumber(event.target.value)}
                    value={number}
                    required />
                </InputGroup>
              </Col>
            </Form.Row>
            <Form.Row>
              <Col>
                <InputGroup size="sm">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-lg" >會員類別</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
                  onChange = {(event)=> setMemberClass(event.target.value)} value={memberClass}
                  as="select" >
                    <option > 批發</option>
                    <option > 團購</option>
                  </FormControl>
                </InputGroup>
              </Col>
             </Form.Row>
             <Form.Row>
	              <Col>
                    <InputGroup size="sm">
                      <InputGroup.Prepend>
                        <InputGroup.Text id="inputGroup-sizing-lg" >姓名</InputGroup.Text>
                      </InputGroup.Prepend>
                     <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
                      onChange = {(event)=> setName(event.target.value)}
                      value={name}
                      required/>
                    </InputGroup>
	              </Col>
              </Form.Row>
              <Form.Row>
	              <Col>
	                <InputGroup size="sm">
	                  <InputGroup.Prepend>
	                    <InputGroup.Text id="inputGroup-sizing-lg" >FB名稱</InputGroup.Text>
	                  </InputGroup.Prepend>
	                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
	                  onChange = {(event)=> setFbName(event.target.value)}
	                  value={fbName}/>
	                </InputGroup>
	              </Col>
	            </Form.Row>
	            <Form.Row>
	              <Col>
	                <InputGroup size="sm">
	                  <InputGroup.Prepend>
	                    <InputGroup.Text id="inputGroup-sizing-lg" >Line名稱</InputGroup.Text>
	                  </InputGroup.Prepend>
	                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
	                  onChange = {(event)=> setLName(event.target.value)}
	                  value={lName}/>
	                </InputGroup>
	              </Col>
	            </Form.Row>
	            <Form.Row>
	              <Col >
	                <InputGroup size="sm">
	                  <InputGroup.Prepend>
	                    <InputGroup.Text id="inputGroup-sizing-lg" >電話</InputGroup.Text>
	                  </InputGroup.Prepend>
	                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
	                  onChange = {(event)=> setPhone(event.target.value)}
	                  value={phone} required/>
	                </InputGroup>
	              </Col>
	            </Form.Row>
	            <Form.Row>
	              <Col>
	                <InputGroup size="sm">
	                  <InputGroup.Prepend>
	                    <InputGroup.Text id="inputGroup-sizing-lg" >通訊地址</InputGroup.Text>
	                  </InputGroup.Prepend>
	                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
	                  onChange = {(event)=> setAddress(event.target.value)}
	                  value={address}/>
	                </InputGroup>
	              </Col>
							</Form.Row>
							<Form.Row>
	              <Col>
	                <InputGroup size="sm">
	                  <InputGroup.Prepend>
	                    <InputGroup.Text id="inputGroup-sizing-lg" >寄貨地址</InputGroup.Text>
	                  </InputGroup.Prepend>
	                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
	                  onChange = {(event)=> setDestination(event.target.value)}
	                  value={destination}/>
	                </InputGroup>
	              </Col>
						</Form.Row>
						<Form.Row>
              <Col>

                <InputGroup size="sm">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-lg" >生日</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
                  type="date" 
                  onChange = {(event)=> {setBirthday(event.target.value)}}
                  value={birthday}/>
                </InputGroup>
              </Col>
            </Form.Row>
            <Form.Row>
              <Col >
                <InputGroup size="sm">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-lg" >信箱</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
                  onChange = {(event)=> setEmail(event.target.value)}
                  value={email}/>
                </InputGroup>
              </Col>
            </Form.Row>
            <Form.Row>
              <Col >
                <InputGroup size="sm">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-lg" >購物金</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
                  onChange = {(event)=> setShopGold(event.target.value)}
                  value={shopGold}/>
                </InputGroup>
              </Col>
            </Form.Row>
            <Form.Row>
              <Col >
                <InputGroup size="sm">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-lg" >取貨方式</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
                  onChange = {(event)=> setMethod(event.target.value)}
                  value={method}
                  />
                </InputGroup>
              </Col>
            </Form.Row>
            <Form.Row>
              <Col >
                <InputGroup size="sm">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-lg" >備註</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
                  onChange = {(event)=> setRemark(event.target.value)}
                  value={remark}/>
                </InputGroup>
              </Col>
            </Form.Row>
            <Button type="submit" onClick={() => pushMember()}>儲存</Button>
        </Form>
      </Col>
    </Row>
    <Modal
      size="xl"
      show={MemberListShow}
      onHide={() => setMemberListShow(false)}
      aria-labelledby="example-modal-sizes-title-lg"
      align="center"
    >  
          <h1>會員清單</h1>     

      <Modal.Body>
                      <hr style={ {border : '1px solid #adb5bd'}}></hr>
          <InputGroup size="sm">
          <InputGroup.Prepend>
            <InputGroup.Text id="inputGroup-sizing-lg" >搜尋會員</InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
          onChange = {(event)=> setsearchStr(event.target.value)} value={searchStr}/>
          </InputGroup>
        <Table striped bordered hover>
          <thead>
            <tr align="center">
              <th>會員編號</th>
              <th>會員類別</th>
              <th>姓名</th>
              <th>Facebook ID</th>
              <th>Line ID</th>
              <th>電話</th>
              <th>通訊地址</th>
              <th>寄貨地址</th>
              <th>生日</th>
              <th>信箱</th>
              <th>購物金</th>
              <th>取貨方式</th>
              <th>備註</th>
              <th>選</th>
              <th>刪</th>
            </tr>
          </thead>
          <tbody>
          {
          	members.map( (item, index) => {

          		return(
          				<tr key={index}>
										<td>{item.number} </td>
										<td>{item.member_class} </td>
										<td>{item.name}</td>
										<td>{item.fb_name} </td>
										<td>{item.l_name} </td>
										<td>{item.phone} </td>
										<td>{item.address} </td>
										<td>{item.destination} </td>
										<td>{item.birthday} </td>
										<td>{item.shop_gold} </td>
										<td>{item.email} </td>
										<td>{item.method} </td>
										<td>{item.remark} </td>
										<td><Button onClick={()=> pullMember(item, index)}>選</Button> </td>
                    <td><Button variant="danger" onClick={()=> removeMember(index, {state:members, setState: setMembers})}>刪</Button> </td>
          				</tr>
          			)
          	})
          }
          </tbody>
        </Table>
      </Modal.Body>
    </Modal>
		</>
	)

}

export default MemberRecord;